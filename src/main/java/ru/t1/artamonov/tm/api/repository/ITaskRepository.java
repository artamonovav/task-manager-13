package ru.t1.artamonov.tm.api.repository;

import ru.t1.artamonov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clearAll();

    Task create(String name, String description);

    Task create(String name);

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Integer getSize();

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}

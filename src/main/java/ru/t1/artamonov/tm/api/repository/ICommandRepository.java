package ru.t1.artamonov.tm.api.repository;

import ru.t1.artamonov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}

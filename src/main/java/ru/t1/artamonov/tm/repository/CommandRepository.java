package ru.t1.artamonov.tm.repository;

import ru.t1.artamonov.tm.api.repository.ICommandRepository;
import ru.t1.artamonov.tm.constant.ArgumentConst;
import ru.t1.artamonov.tm.constant.TerminalConst;
import ru.t1.artamonov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT,
            "Display developer info."
    );

    private static final Command EXIT = new Command(TerminalConst.CMD_EXIT, null,
            "Close application."
    );

    private static final Command HELP = new Command(TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP,
            "Display list of terminal commands."
    );

    private static final Command INFO = new Command(TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO,
            "Display info."
    );

    private static final Command VERSION = new Command(TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION,
            "Display program version."
    );

    private static final Command COMMANDS = new Command(TerminalConst.CMD_COMMANDS, ArgumentConst.ARG_COMMANDS,
            "Display program commands."
    );

    private static final Command ARGUMENTS = new Command(TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARG_ARGUMENTS,
            "Display program arguments."
    );

    private static final Command PROJECT_CREATE = new Command(TerminalConst.CMD_PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(TerminalConst.CMD_PROJECT_LIST, null,
            "Display project list."
    );

    private static final Command PROJECT_CLEAR = new Command(TerminalConst.CMD_PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private static final Command TASK_CREATE = new Command(TerminalConst.CMD_TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_LIST = new Command(TerminalConst.CMD_TASK_LIST, null,
            "Display task list."
    );

    private static final Command TASK_CLEAR = new Command(TerminalConst.CMD_TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_SHOW_BY_INDEX, null,
            "Display project by index."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX, null,
            "Update project by index."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX, null,
            "Remove project by index."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(TerminalConst.CMD_PROJECT_SHOW_BY_ID, null,
            "Display project by id."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(TerminalConst.CMD_PROJECT_UPDATE_BY_ID, null,
            "Update project by id."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(TerminalConst.CMD_PROJECT_REMOVE_BY_ID, null,
            "Remove project by id."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX, null,
            "Change project status by index."
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_START_BY_INDEX, null,
            "Start project by index."
    );

    private static final Command PROJECT_COMPLETE_BY_INDEX = new Command(TerminalConst.CMD_PROJECT_COMPLETE_BY_INDEX, null,
            "Complete project by index."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_ID, null,
            "Change project status by id."
    );

    private static final Command PROJECT_START_BY_ID = new Command(TerminalConst.CMD_PROJECT_START_BY_ID, null,
            "Start project by id."
    );

    private static final Command PROJECT_COMPLETE_BY_ID = new Command(TerminalConst.CMD_PROJECT_COMPLETE_BY_ID, null,
            "Complete project by id."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(TerminalConst.CMD_TASK_SHOW_BY_INDEX, null,
            "Display task by index."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(TerminalConst.CMD_TASK_UPDATE_BY_INDEX, null,
            "Update task by index."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(TerminalConst.CMD_TASK_REMOVE_BY_INDEX, null,
            "Remove task by index."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(TerminalConst.CMD_TASK_SHOW_BY_ID, null,
            "Display task by id."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(TerminalConst.CMD_TASK_UPDATE_BY_ID, null,
            "Update task by id."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(TerminalConst.CMD_TASK_REMOVE_BY_ID, null,
            "Remove task by id."
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(TerminalConst.CMD_TASK_CHANGE_STATUS_BY_INDEX, null,
            "Change task status by index."
    );

    private static final Command TASK_START_BY_INDEX = new Command(TerminalConst.CMD_TASK_START_BY_INDEX, null,
            "Start task by index."
    );

    private static final Command TASK_COMPLETE_BY_INDEX = new Command(TerminalConst.CMD_TASK_COMPLETE_BY_INDEX, null,
            "Complete task by index."
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(TerminalConst.CMD_TASK_CHANGE_STATUS_BY_ID, null,
            "Change task status by id."
    );

    private static final Command TASK_START_BY_ID = new Command(TerminalConst.CMD_TASK_START_BY_ID, null,
            "Start task by id."
    );

    private static final Command TASK_COMPLETE_BY_ID = new Command(TerminalConst.CMD_TASK_COMPLETE_BY_ID, null,
            "Complete task by id."
    );

    private static final Command TASK_BIND_TO_PROJECT = new Command(TerminalConst.CMD_TASK_BIND_TO_PROJECT, null,
            "Bind task to project."
    );

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(TerminalConst.CMD_TASK_UNBIND_FROM_PROJECT, null,
            "Unbind task from project."
    );

    private static final Command TASK_SHOW_BY_PROJECT_ID = new Command(TerminalConst.CMD_TASK_SHOW_BY_PROJECT_ID, null,
            "Show task list by project id."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, VERSION, HELP,
            ARGUMENTS, COMMANDS,

            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_ID,
            PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_CHANGE_STATUS_BY_ID,
            PROJECT_START_BY_INDEX, PROJECT_START_BY_ID,
            PROJECT_COMPLETE_BY_INDEX, PROJECT_COMPLETE_BY_ID,

            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_ID,
            TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_ID,
            TASK_START_BY_INDEX, TASK_START_BY_ID,
            TASK_COMPLETE_BY_INDEX, TASK_COMPLETE_BY_ID,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT, TASK_SHOW_BY_PROJECT_ID,

            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
